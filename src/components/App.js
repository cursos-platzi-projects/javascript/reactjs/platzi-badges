import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Layout from './Layout';

import BadgeEdit from '../pages/BadgeEdit';
import BadgeNew from '../pages/BadgeNew';
import Badges from '../pages/Badges';
import BadgeDetailsContainer from '../pages/BadgeDetailsContainer';
import NotFound from '../pages/NotFound';

function App() {
	return (
		<BrowserRouter>
			<Layout>
				<Switch>
					<Route exact path="/badges" component={Badges} />
					<Route exact path="/badges/new" component={BadgeNew} />
					<Route exact path="/badges/:badgeId" component={BadgeDetailsContainer} />
					<Route exact path="/badges/:badgeId/edit" component={BadgeEdit} />
					<Route component={NotFound} />
				</Switch>
			</Layout>
		</BrowserRouter>
	);
}

export default App;

/* 
Este componente se dedica a setear las rutas que tiene la aplicacion para que esta se convierta
en una SPA
El componente que no tiene un path es un componente para mostrar un mensaje de
"pagina no encontrado, 404".
*/