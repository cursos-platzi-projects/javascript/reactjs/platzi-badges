import React from 'react';
import {Link} from 'react-router-dom';
//import './styles/BadgesList.css';

/*class BadgesListItem extends React.Component {
	render() {
		return (
		<div className="BadgesListItem">
			<Gravatar
			className="BadgesListItem__avatar"
			email={this.props.badge.email}
			/>

			<div>
			<strong>
				{this.props.badge.firstName} {this.props.badge.lastName}
			</strong>
			<br />@{this.props.badge.twitter}
			<br />
			{this.props.badge.jobTitle}
			</div>
		</div>
		);
	}
}*/

//Esto es un customHook
function useSearchBadges(badges) {
	//React.useMemo = Calcula por primera vez los datos pero ya una vez que los tenga ya no los vuelve a calcular
	//Esto puede ser tomado o parecido a la cache
	const [query, setQuery] = React.useState('')
	const [ filteredBadges, setFilteredBadges ] = React.useState(badges);
	React.useMemo(() => {
		const result = badges.filter(badge => {
		return badge.firstName.toLowerCase().includes(query.toLowerCase());
	});

		setFilteredBadges(result)
	}, [badges, query]);

	return { query, setQuery, filteredBadges }
}

function BadgesList (props) {
	const badges = props.badges
	const { query, setQuery, filteredBadges } = useSearchBadges(badges);

	//if(badges.length === 0) {
		if(filteredBadges.length === 0) {
		return (
			<div>
				<div className="form-group">
					<label>Filter Badges</label>
					<input type="text" className="form-control"
						value={query}
						onChange={(e) => {
							setQuery(e.target.value);
						}}
					/>
				</div>

				<h3>No badges were found</h3>
				<Link className="btn btn-primary" to="/badges/new">
					Create new badge
				</Link>
			</div>
		)
	}


	return (
		<div className="BadgesList">
			<div className="form-group">
				<label>Filter Badges</label>
				<input type="text" className="form-control"
					value={query}
					onChange={(e) => {
						setQuery(e.target.value);
					}}
				/>
			</div>

			<ul className="list-unstyled">
				{filteredBadges.map(badge => {
					return (
						<li key={badge.id}>
							<Link className="text-reset text-decoration-none" to={`/badges/${badge.id}`}>
								<p>{badge.firstName} {badge.lastName}</p>
							</Link>
						</li>
					);
				})}
			</ul>
		</div>
		
	);
}

export default BadgesList;

//Este componente muestra una lista de usuarios en la pagina de /badges

/**
 * Link = se usa este componente en lugar del <a> para redigir y cargar la pagina como una SPA
 */

 /**
  * map = la funcion ayuda a recorrer una lista de objetos
  */