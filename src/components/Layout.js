import React from 'react';
import Navbar from './Navbar';

function Layout(props) {
	return (
		<React.Fragment>
			<Navbar />
			{props.children}
		</React.Fragment>
	);
}

export default Layout;

//Este componente ayuda a mostrar el header en todas paginas sin tener que repetir el componente
//en cada uno de ellos

/**
 * {props.children} = para mostrar el contenido.
 */

 /**
 * React.Fragment = Se usa esto en lugar de un div para mejor vista en el codigo.
 */