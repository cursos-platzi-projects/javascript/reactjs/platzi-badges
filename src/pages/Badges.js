import React from 'react';
import { Link } from 'react-router-dom';

import ConfLogo from '../images/badge-header.svg';
import BadgestList from '../components/BadgesList';
import PageLoading from '../components/PageLoading';
import PageError from '../components/PageError';
import MiniLoader from '../components/MiniLoader';
import './styles/Badges.css';

import api from '../api';

class Badges extends React.Component {

		state = {
			loading: true,
			error: null,
			data: undefined,
		};

		componentDidMount() {
			//Va a comenzar con la peticion
			this.fetchData()

			//A esto se le llama pollin, ejecucion del metodo en background cada cierto tiempo
			//Esto es mejor que hacer un websocket y no hacer un overkilling, haciendo mas proceso del necesario
			//Asingnando un valor id del interlar para usarlo con la funcion clearInterval
			this.intervalId = setInterval(this.fetchData, 5000)
		}

		//Cuando el componente se destruya o se vaya se ejecuta este metodo
		componentWillUnmount() {
			//Para que ya no se este ejecutando el interval cuando el componente ya no exista
			clearInterval(this.intervalId)
		}

		fetchData = async () => {
			this.setState({loading: true, error: null})

			try {
				//Si podemos obtener los datos
				const data = await api.badges.list();
				//const data = [];
				this.setState({loading: false, data: data})
			} catch(error) {
				this.setState({loading: false, error: error})
			}
		}

    render() {
			if(this.state.loading === true && this.state.data === undefined) {
				return <PageLoading />;
			}

			//Cuando hay un error mostrarlo
			if(this.state.error) {
				return <PageError error={this.state.error} />;
			}

			return(
				<React.Fragment>
					<div className="Badges">
						<div className="Badges__hero">
							<div className="Badges__container">
								<img className="Badges__conf-logo" src={ConfLogo} alt="Conf logo" />
							</div>
						</div>
					</div>

					<div className="Badge__container">
						<div className="Badges__buttons">
							<Link to="/badges/new" className="btn btn-primary">New Badge</Link>
						</div>

						<div className="Badges__list">
							<div className="Badges__container">

								{this.state.loading && <MiniLoader />}

								<BadgestList badges={this.state.data} />

							</div>
						</div>
					</div>
				</React.Fragment>
			);
    }
}

export default Badges;

//Este componente muestra toda la pagina con sus componente: /badges
