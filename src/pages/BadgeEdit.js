import React from 'react';
import header from '../images/platziconf-logo.svg'
import './styles/BadgeEdit.css';
import Badge from '../components/Badge';
import BadgeForm from '../components/BadgeForm';
import PageLoading from '../components/PageLoading';
import api from '../api';

class BadgeEdit extends React.Component {
    state = { 
				loading: true,
				error: null,
        form: {
					firstName: '',
					lastName: '',
					email: '',
					jobTitle: '',
					twitter: '',
		} };
		
		componentDidMount() {
			this.fetchData()
		}

		fetchData = async e => {
			this.setState({ loading: true, error: null })

			try {
				const data = await api.badges.read(
					//De esta manera se obtiene el id que estas obteniendo por get
					//El nombre del parametro badgeId es el mismo con el que seteaste en App para la ruta
					this.props.match.params.badgeId
				)

				this.setState({ loading: false, form: data })
			} catch (error) {
				this.setState({ loading: false, error: error })
			}
		};

    handleChange = e => {
        this.setState({
            form: {
                ... this.state.form,
                [e.target.name]: e.target.value,
            },
        });
    };

    handleSubmit = async e => {
        e.preventDefault() // para detener el submit
        this.setState({loading: true, error: null})

        try {
            await api.badges.update(this.props.match.params.badgeId, this.state.form)
						this.setState({ loading: false })
						
						//Hacer una redireccion a la pagina /badges 
						this.props.history.push('/badges');
        } catch (error) {
            this.setState({ loading: false, error: error })
        }

    }

    render() {
				if (this.state.loading) {
					return <PageLoading />
				}

        return (
            <React.Fragment>
                <div className="BadgeEdit__hero">
                    <img className="BadgeEdit__hero-image img-fluid" src={header} alt="Logo" />
                </div>

                <div className="container">
                    <div className="row">
                        <div className="col-6">

                        <Badge 
                            firstName={this.state.form.firstName || 'FIRST_NAME'} 
                            lastName={this.state.form.lastName || 'LAST_NAME'}
                            jobTitle={this.state.form.jobTitle || 'JOB_TITLE'}
                            twitter={this.state.form.twitter || 'twitter'}
                            email={this.state.form.email || 'EMAIL'}
                            avatarURL="https://www.gravatar.com/avatar?d=identicon"/>

                        </div>

                        <div className="col-6">
													<h1>Edit Attendant</h1>
													<BadgeForm 
															onChange={this.handleChange}
															onSubmit={this.handleSubmit}
															formValues={this.state.form}
															error={this.state.error} />
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default BadgeEdit;

//Este componente muestra toda la pagina con sus componente: /badges/new